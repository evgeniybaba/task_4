<?php
declare(strict_types=1);

function sum(string $numberA, string $numberB): string
{
    $result = '';
    $bank = 0;
    $maxLength = getMaxLength($numberA, $numberB);
    ++$maxLength;
    $numberA = setLengthForNumber($numberA, $maxLength);
    $numberB = setLengthForNumber($numberB, $maxLength);

    for ($i = $maxLength - 1; $i >= 0; --$i) {
        $sub_result = (int)$numberA[$i] + (int)$numberB[$i] + $bank;
        $bank = 0;
        if ($sub_result > 9) {
            $sub_result = ((string)$sub_result)[1];
            $bank = 1;
        }

        $result = (string)$sub_result . $result;
    }

    if($result[0] === '0') {
        $result = substr($result, 1);
    }

    return $result;
}

function getMaxLength(string $numberA, string $numberB): int
{
    $lengthNumberA = strlen($numberA);
    $lengthNumberB = strlen($numberB);
    return $lengthNumberA > $lengthNumberB
        ? $lengthNumberA
        : $lengthNumberB;
}

function setLengthForNumber(string $numberA, int $length): string
{
    return str_repeat('0', $length - strlen($numberA)) . $numberA;
}

$numberA = '123456789123456789';
$numberB = '123456789';
$sum = '123456789246913578';
/* Expected output: 1 */
echo 'One case' . PHP_EOL;
echo sum($numberA, $numberB) === $sum ? 'Success': 'Fail';
echo PHP_EOL;
$numberA = '99';
$numberB = '99';
$sum = '198';
/* Expected output: 1 */
echo 'Two case' . PHP_EOL;
echo sum($numberA, $numberB) === $sum ? 'Success': 'Fail';
echo PHP_EOL;
$numberA = '0';
$numberB = '0';
$sum = '0';
/* Expected output: 1 */
echo 'Three case' . PHP_EOL;
echo sum($numberA, $numberB) === $sum ? 'Success': 'Fail';
echo PHP_EOL;